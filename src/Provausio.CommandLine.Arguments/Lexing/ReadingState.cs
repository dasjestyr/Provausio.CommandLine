﻿namespace Provausio.CommandLine.Arguments.Lexing
{
    internal enum ReadingState
    {
        STARTING,
        READING_SHORTNAME_MARKER,
        READING_LONGNAME_MARKER,
        READING_NAME,
        READING_VALUE,
        READING_QUOTED_VALUE,
        CLOSING_QUOTE,
        READING_NON_VALUE
    }
}