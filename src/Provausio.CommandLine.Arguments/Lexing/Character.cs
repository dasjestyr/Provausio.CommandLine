﻿namespace Provausio.CommandLine.Arguments.Lexing
{
    internal static class Character
    {
        public static CharacterType GetType(int c)
        {
            return GetType((char) c);
        }

        public static CharacterType GetType(char c)
        {
            CharacterType type;
            switch (c)
            {
                case '-':
                    type = CharacterType.MARKER;
                    break;
                case '"':
                    type = CharacterType.QUOTE;
                    break;
                case ' ':
                    type = CharacterType.SPACE;
                    break;
                case '=':
                    type = CharacterType.EQUALS;
                    break;
                default:
                    type = CharacterType.VALUE;
                    break;
            }

            return type;
        }
    }
}