﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Provausio.CommandLine.Arguments.Lexing
{
    public class ArgumentParser
    {
        private readonly StringBuilder _argumentBuilder = new StringBuilder();
        private readonly StringBuilder _argumentValueBuilder = new StringBuilder();

        private bool _verbsAreRead;
        private ReadingState _currentReadingState = ReadingState.STARTING;
        private BuildingState _buildingState = BuildingState.STARTING;

        public IEnumerable<KeyValuePair<string, object>> Read(string argumentString)
        {
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(argumentString));
            var sr = new StreamReader(stream);

            var currentByte = sr.Read();
            while (currentByte > -1)
            {
                var type = Character.GetType(currentByte);
                switch (type)
                {
                    case CharacterType.MARKER:
                        _verbsAreRead = true;
                        ProcessMarker();
                        break;
                    case CharacterType.VALUE:
                        if (ProcessValue(currentByte, sr))
                            yield return FlushBuffer();
                        break;
                    case CharacterType.SPACE:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                currentByte = sr.Read();
            }
        }

        private void ProcessMarker()
        {
            _buildingState = BuildingState.BUILDING_ARGUMENT;
            switch (_currentReadingState)
            {
                case ReadingState.READING_SHORTNAME_MARKER:
                    _currentReadingState = ReadingState.READING_LONGNAME_MARKER;
                    break;
                case ReadingState.READING_LONGNAME_MARKER:
                    throw new FormatException("Invalid argument name.");
                default:
                    _currentReadingState = ReadingState.READING_SHORTNAME_MARKER;
                    break;
            }
        }

        private void ReadArgValue(
            int currentCharacter,
            CharacterType breakValue,
            TextReader sr)
        {
            _currentReadingState = ReadingState.READING_VALUE;
            if (currentCharacter == (int) CharacterType.QUOTE)
            {
                currentCharacter = sr.Read();
                while (currentCharacter != (int) CharacterType.QUOTE)
                {
                    _argumentValueBuilder.Append((char)currentCharacter);
                    currentCharacter = sr.Read();
                }

                if(sr.Peek() != (int) CharacterType.SPACE)
                    throw new FormatException("Cannot have additional info outside of the quote when declaring an argument value.");
            }
            else
            {
                while (currentCharacter != (int) breakValue && currentCharacter != -1)
                {
                    _argumentValueBuilder.Append((char)currentCharacter);
                    currentCharacter = sr.Read();
                }
            }
        }

        private bool ProcessValue(int character, TextReader sr)
        {
            // what was last thing we encountered?
            switch (_currentReadingState)
            {
                // reads --arg=some-value format
                case ReadingState.READING_LONGNAME_MARKER:
                    _buildingState = BuildingState.BUILDING_ARGUMENT;
                    while (character != (int) CharacterType.EQUALS && character != -1)
                    {
                        _currentReadingState = ReadingState.READING_VALUE;
                        _argumentBuilder.Append((char) character);
                        character = sr.Read();
                    }

                    _buildingState = BuildingState.BUILDING_ARGUMENT_VALUE;
                    character = sr.Read();
                    ReadArgValue(character, CharacterType.SPACE, sr);
                    break;

                // read -f some-value format but watch out for something like -f -bar -o baz
                case ReadingState.READING_SHORTNAME_MARKER:
                    _buildingState = BuildingState.BUILDING_ARGUMENT;
                    while (character != (int) CharacterType.SPACE && character != -1)
                    {
                        _argumentBuilder.Append((char) character);
                        character = sr.Read();
                    }
                    
                    if (sr.Peek() == (int) CharacterType.MARKER)
                        break; // it was a flag
                    
                    if (_argumentBuilder.Length > 1)
                        throw new FormatException("Short named parameters must be 1 character only.");

                    var nextCharacter = sr.Read();

                    _buildingState = BuildingState.BUILDING_ARGUMENT_VALUE;
                    ReadArgValue(nextCharacter, CharacterType.SPACE, sr);
                    break;
                case ReadingState.STARTING:
                    if(_currentReadingState == ReadingState.STARTING && _verbsAreRead)
                        throw new FormatException("Invalid args string. If you included a verb, verbs must be first");

                    while (character != (int) CharacterType.SPACE && character != -1)
                    {
                        _argumentBuilder.Append((char) character);
                        character = sr.Read();
                        _currentReadingState = ReadingState.READING_VALUE;
                    }
                    break;
                default:
                    throw new InvalidOperationException("Invalid state.");
            }

            ResetState();
            return true;
        }

        private void ResetState()
        {
            _buildingState = BuildingState.STARTING;
            _currentReadingState = ReadingState.STARTING;
        }
        
        private KeyValuePair<string, object> FlushBuffer()
        {
            // for flags and verbs, simply return a boolean

            var argument = _argumentBuilder.ToString();

            var kvp = _argumentValueBuilder.Length == 0 
                ? new KeyValuePair<string, object>(argument, true) 
                : new KeyValuePair<string, object>(argument, _argumentValueBuilder.ToString());

            _argumentBuilder.Clear();
            _argumentValueBuilder.Clear();
            return kvp;
        }
    }
}
